from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO
from common.json import ModelEncoder

# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }



@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        hat = Hat.objects.get(id=pk) #pk is passed from the browser to the view function
        return JsonResponse(
                {"hat": hat},
                encoder=HatDetailEncoder,
                safe=False,
            )




class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST", "DELETE"])
def api_list_hats(request,):

    if request.method == "GET":
            hats = Hat.objects.all() # gets all objects(Hats)from Hat model
            return JsonResponse(
                {"hats": hats},
                encoder=HatListEncoder,
            )
    else:
        content = json.loads(request.body)
        location_href = content['location']
        location = LocationVO.objects.get(import_href=location_href)
        content['location'] = location
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )


# [] returns a JSON Response
                                    # JSON Respons(data, encoder=DjangoJSONEncoder, safe=True, json_dumps_params=None, **kwargs)
                                    # [] data should be a dictionary
                                    # [] the encoder is used to serialize the data
                                    # if safe=False any object can be passed for serialization
                                    # json_dumps_params is a dictionary of keyword arguments to pass to the json.dumps() call used to generate the response.
