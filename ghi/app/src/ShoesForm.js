import React from 'react';

class ShoesForm extends React.Component {
    // define shoe props
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bin: [],
        };
        this.handleManufacturerChange = this.handleanufacturerChange.bind(this);
        this.handleModel_NameChange = this.handleModel_NameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePicture_UrlChange = this.handlePicture_UrlChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    // [] define creation/POST
    async handleSubmit(event) {
        event.preventDefault();

        const data = {...this.state};
        delete data.picture_url;
        delete data.bin;
        console.log("DATA:::", data);

        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
        const newShoe = await response.json();
        console.log("NEW:::", newShoe);

        const cleared = {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bin: '',
        };
        this.setState(cleared);
        }
    }

    // set state for each prop
    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
    }

    handleModel_NameChange(event) {
        const value = event.target.value;
        this.setState({model_name: value})
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }

    handlePicture_UrlChange(event) {
        const value = event.target.value;
        this.setState({picture_url: value});
    }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({bin: value});
    }

    // [] add options for bin drop down
    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        this.setState({bins: data.bins});
        }
    }

    // [] render return for form
    render() {
        return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new location</h1>
                <form onSubmit={this.handleSubmit} id="create-shoes-form">
                <div className="form-floating mb-3">
                    <input value={this.state.manufacturer} onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={this.state.model_name} onChange={this.handleModel_NameChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                    <label htmlFor="room_model">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={this.state.picture_url} onChange={this.handlePicture_UrlChange} placeholder="Picture Url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                {/* <div className="mb-3">
                    <select value={this.state.state} onChange={this.handleStateChange} required name="state" id="state" className="form-select">
                    <option value="">Choose a state</option>
                    {this.state.states.map(state => {
                        return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                        );
                    })}
                    </select>
                </div> */}
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
    }
}

export default ShoesForm;