import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
);

// pull shoes data
async function loadShoes() {
        const response = await fetch("http://localhost:8080/api/shoes/");
        console.log(response);
    
        if (response.ok) {
            let data = await response.json();
            console.log("DATA: ", data);
            root.render(
                <React.StrictMode>
                    <App shoes={data.shoes}/>
                </React.StrictMode>
            );
        }
        
        else {
            console.error("response not ok", response);
        }
    }
    
loadShoes();
  