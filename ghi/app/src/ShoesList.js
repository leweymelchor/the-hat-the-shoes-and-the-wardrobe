import React from 'react';
import { NavLink } from 'react-router-dom';

function ShoesList(props) {
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Shoe</th>
                        <th>Closet Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map(shoe => {
                        return (
                            <tr key={shoe.href}>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.picture_url}</td>
                                {/* NEED TO CREATE FUNCTIONALITY & LINK FOR DELETE
                                <td><button onClick={() => this.delete(shoe.id)}>Delete</button></td> */}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            {/*  NEED TO LINK NEW SHOES FUNCTIONALITY      
            <button>
                <NavLink className="nav-link" to="/shoes/new">New shoes</NavLink>
            </button> */}
        </>
    )

}

export default ShoesList;
