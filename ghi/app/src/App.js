import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
 // !!! import list/ forms from js files

function App(props) {
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />}/> 
                        <Route path="shoes" element={<ShoesList shoes={props.shoes}/>}/>
                        <Route path="shoes/new" element={<ShoesForm />}>
                        {/* !! ADD HATS ROUTES HERE */}
                    </Route>
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
