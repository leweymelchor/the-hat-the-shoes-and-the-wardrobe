from django.db import models

from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField()


class Shoe(models.Model):
    """
    The Shoe model represents one pair of shoes
    with its manufacturer, model name, color, image & bin in the wardrobe.
    """

    manufacturer = models.CharField(max_length=25)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=25)
    picture_url = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
