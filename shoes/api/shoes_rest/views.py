from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe

# > ENCODERS
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "closet_name",
        "bin_number",
        "import_href",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "model_name", "picture_url"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


# > VIEWS
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    """
    Collection RESTful API handler for Shoe objects in
    the wardrobe.
        
    GET: Returns a dictionary with a single key "shoes" which is a
    list of shoes id, model name, & picture URL
    {
        "shoes": [
            {
                "id": database id for the shoe
                "model_name": model of the shoe style
                "picture_url": URL for image of the shoe
            },
        ...
        ]
    }

    POST: Creates a shoe object and returns its details
    {
        "manufacturer": manufacturer of the shoe
        "model_name": model of the shoe style
        "color": color of the shoe
        "picture_url": URL to image of shoe
        "bin": URL to the bin "/api/locations/:id/"
    }
    """
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:    # POST
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    """
    Single-object API for the Shoe resource.

    GET:
    Returns the information for a Shoe resource
    based on the value of pk
        {
            "manufacturer": manufacturer of the shoe
            "model_name": model of the shoe style
            "color": color of the shoe
            "picture_url": URL for image of shoe
            "bin": {
                "id": database id for the bin
                "closet_name": bin's closet name
                "bin_number": bin
                "import_href": URL to the bin "/api/locations/:id/"
            }
        }

    DELETE: Removes the shoe resource from the application
    """
    try:
        if request.method == "GET":
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder = ShoeDetailEncoder,
                safe = False
            )
        elif request.method == "DELETE":
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})

    except Shoe.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid shoe"},
            status=400,
        )
