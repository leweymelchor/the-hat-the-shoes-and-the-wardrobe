
![Our Logo](ghi/app/public/Wardrobify logo 2.png)

# Wardrobify
![logo here](ghi/app/public/Wardrobify logo 2.png)

Team:

1. Lewey - Hats
2. Holly - Shoes

## Overview

___

>Wardrobify is dedicated to providing an elegant and simplified way of maintaining a database of your favorite hats and shoes.
Keep your hats and shoes in a wardrobe of your choosing, it neatly stores images and information such as color, style, manufacturer, and more, about each of your saved items.

## Key Features

___

Utilize our Shoes App to manage your favorite shoes
and take advantage of our Hats App to keep track of your many fashionabe hats
keep them in the same wardrobe, keep them in different wardrobes, it doesnt matter to us, as long as you dont forget to like and subscribe.

## How to Run it
___

### Requirements:

1. Python 3
2. Docker
3. VSCode or Code editor of choice

### Start the app:

1. Open your Docker app
2. Open VSCODE
3. Open The main directory, the-hat-the-shoes-and-the-wardrobe (in VSCode)
4. Open terminal to top level directory (control + `)
5. Run <code> docker-compose build </code>
6. Run <code> docker-compose up </code>
7. Open webbrowser to localhost:3000
8. Enjoy Wardrobify!! Our helpful shoe and hat tracking solution!!

## Design
___

## Shoes microservice (API -> port 8080)

### `http://localhost:8080/api/shoes/`

- `GET`: Returns a dictionary with a single key "shoes" which is a list of shoes id, model name, & picture URL.

    ```json
    {
    "shoes": [
        {
            "id": database id for the shoe,
            "model_name": model of the shoe style,
            "picture_url": URL for image of the shoe
        },
    ...
    ]
    }
    ```

- `POST`: Creates a shoe object and returns its details.

    ```json
    {
        "manufacturer": manufacturer of the shoe,
        "model_name": model of the shoe style,
        "color": color of the shoe,
        "picture_url": URL to image of shoe,
        "bin": URL to the bin "/api/locations/:id/"
    }
    ```
### `http://localhost:8080/api/shoes/:id/`
- `GET` Returns the information for a Shoe resource
    based on the value of pk.

    ```json
    {
        "manufacturer": manufacturer of the shoe,
        "model_name": model of the shoe style,
        "color": color of the shoe,
        "picture_url": URL for image of shoe,
        "bin": {
            "id": database id for the bin,
            "closet_name": bin's closet name,
            "bin_number": bin,
            "import_href": URL to the bin
        }
    }
    ```

- `DELETE` Removes the shoe resource from the application

## Hats microservice (API -> port 8090)


### `http://localhost:8090/api/hats/`
- `GET`: Returns a dictionary with a single key "hats" which is a list of hats id, model name, & picture URL.

    ```json
    {
        "hats": [
            {
                "fabric": fabric of the hat,
                "style": style of the hat,
                "color": color of the hat,
                "picture_url": URL for image of the hat
            },
        ...
        ]
    }
    ```

- `POST`: Creates a hat object and returns its details.

    ```json
    {
        "fabric": fabric of the hat,
        "style": style of the hat,
        "color": color of the hat,
        "picture_url": URL for image of the hat,
    }
    ```
### `http://localhost:8080/api/hats/:id/`
- `GET` Returns the information for a hat resource
    based on the value of pk.

    ```json
    {
        "fabric": fabric of the hat,
        "style": style of the hat,
        "color": color of the hat,
        "picture_url": URL for image of the hat,
        "location": {
            "import_href": URL to the bin,
            "closet_name": name of your wardrobe,
            "section_number": chosen section number,
            "shelf_number": chosen shelf number,
        }
    }
    ```

- `DELETE` Removes the hats resource from the application

___

## Wardrobe microservice

Poller base URL -> `http://wardrobe-api:8000`

### `http://localhost:8100/api/locations/`

- `GET` Returns a dictionary with a single key "locations" which is a list of the closet name, section number, and shelf number for the location, along with its href and id.

    ```json
    {
        "locations": [
            {
                "id": database id for the location,
                "closet_name": location's closet name,
                "section_number": the number of the wardrobe section,
                "shelf_number": the number of the shelf,
                "href": URL to the location,
            },
            ...
        ]
    }
    ```

- `POST` Creates a location resource and returns its details.

    ```json
    {
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
    }
    ```


### `http://localhost:8100/api/locations/:id/`

- `GET` Returns the information for a Location resource based on the value of pk

    ```json
    {
        "id": database id for the location,
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
        "href": URL to the location,
    }
    ```

- `PUT` Updates the information for a Location resource based on the value of the pk

    ```json
    {
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
    }
    ```

- `DELETE` Removes the location resource from the application

### `http://localhost:8100/api/bins/`

- `GET` Returns a dictionary with a single key "bins" which is a list of the closet name, bin number, and bin size for the bin, along with its href and id.

    ```json
    {
        "bins": [
            {
                "id": database id for the bin,
                "closet_name": bin's closet name,
                "bin_number": the number of the bin,
                "bin_size": the size of the bin,
                "href": URL to the bin,
            },
            ...
        ]
    }
    ```

- `POST` Creates a bin resource and returns its details.

    ```json
    {
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
    }
    ```


### `http://localhost:8100/api/bins/:id/`

- `GET` Returns the information for a Bin resource based on the value of pk

    ```json
    {
        "id": database id for the bin,
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
        "href": URL to the bin,
    }
    ```

- `PUT` Updates the information for a Bin resource based
    on the value of the pk

    ```json
    {
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
    }
    ```

- `DELETE` Removes the bin resource from the application

## Wardrobe API

___

### `/api/locations/`

- `GET` Returns a dictionary with a single key "locations" which is a list of the closet name, section number, and shelf number for the location, along with its href and id.

    ```json
    {
        "locations": [
            {
                "id": database id for the location,
                "closet_name": location's closet name,
                "section_number": the number of the wardrobe section,
                "shelf_number": the number of the shelf,
                "href": URL to the location,
            },
            ...
        ]
    }
    ```

- `POST` Creates a location resource and returns its details.

    ```json
    {
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
    }
    ```


### `/api/locations/:id/`

- `GET` Returns the information for a Location resource based on the value of pk

    ```json
    {
        "id": database id for the location,
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
        "href": URL to the location,
    }
    ```

- `PUT` Updates the information for a Location resource based on the value of the pk

    ```json
    {
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
    }
    ```

- `DELETE` Removes the location resource from the application

### `/api/bins`

- `GET` Returns a dictionary with a single key "bins" which is a list of the closet name, bin number, and bin size for the bin, along with its href and id.

    ```json
    {
        "bins": [
            {
                "id": database id for the bin,
                "closet_name": bin's closet name,
                "bin_number": the number of the bin,
                "bin_size": the size of the bin,
                "href": URL to the bin,
            },
            ...
        ]
    }
    ```

- `POST` Creates a bin resource and returns its details.

    ```json
    {
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
    }
    ```


### `/api/bins/:id/`

- `GET` Returns the information for a Bin resource based on the value of pk

    ```json
    {
        "id": database id for the bin,
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
        "href": URL to the bin,
    }
    ```

- `PUT` Updates the information for a Bin resource based
    on the value of the pk

    ```json
    {
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
    }
    ```

- `DELETE` Removes the bin resource from the application

## If you read this far
- dont forget to give us an A!
